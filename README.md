SwiftAsignment

NYC HighSchools
Description
A simple iOS Native app developed in Swift to hit the NYCHighSchools API and show a list of schools, that shows details when items on the list are tapped (a typical master/detail app).

Demonstrations
Covers the following:

•	  Object Oriented Programming Approach
•	  MVVM Approach
•	  Unit Tests
•	  Simple UI Test by Record UI Test
•	  Generic and simple code


App Features:

•	  SingleView applciation
•	  Auto layout with Dynamic Cell Resizing
•	  Dynamic school detail items
